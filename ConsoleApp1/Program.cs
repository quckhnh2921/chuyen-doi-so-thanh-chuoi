﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
void ChuyenDoiSoThanhChuoi(int number)
{
    var mangKyTu = number.ToString().ToCharArray();
    int xetSoLuong = mangKyTu.Length;
    switch (xetSoLuong)
    {
        case 1:
            Console.WriteLine(SoHangDonVi(number));
            break;
        case 2:
            Console.WriteLine(SoHangChuc(number));
            break;
        case 3:
            Console.WriteLine(SoHangTram(number));
            break;
        default:
            break;
    }
}
ChuyenDoiSoThanhChuoi(225);
string SoHangTram(int number)
{
    string hangTram = " trăm ";
    string hangChuc = " mươi ";
    string hangChucDacBiet = " mười ";
    string kyTuOGiuaLaKhong = " lẻ ";
    string kyTudacBiet = " mốt";
    var kyTu = number.ToString().ToCharArray();
    switch (kyTu[1])
    {
        case '0':
            if (kyTu[2] == '0')
            {
                return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram;
            }
            return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + kyTuOGiuaLaKhong + SoHangDonVi(int.Parse(kyTu[2].ToString()));
        case '1':
            if (kyTu[2] == '0')
            {
                return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + hangChucDacBiet;
            }
            if (kyTu[2] == '5' && kyTu[1] == '1')
            {
                return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + hangChucDacBiet + "lăm";
            }
            if (kyTu[2] == '5')
            {
                return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + SoHangDonVi(int.Parse(kyTu[1].ToString())) + hangChuc + "lăm";
            }
            return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + hangChucDacBiet + SoHangDonVi(int.Parse(kyTu[2].ToString()));
        default:
            if (kyTu[2] == '1')
            {
                return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + SoHangDonVi(int.Parse(kyTu[1].ToString())) + hangChuc + kyTudacBiet;
            }
            if (kyTu[2] == '0')
            {
                return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + SoHangDonVi(int.Parse(kyTu[1].ToString())) + hangChuc;
            }
            if (kyTu[2] == '5')
            {
                return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + SoHangDonVi(int.Parse(kyTu[1].ToString())) + hangChuc + "lăm";
            }
            return SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangTram + SoHangDonVi(int.Parse(kyTu[1].ToString())) + hangChuc + SoHangDonVi(int.Parse(kyTu[2].ToString()));
    }
}

string SoHangChuc(int number)
{
    string hangChuc = " mươi ";
    string ketQua = "";
    string kyTudacBiet = " mốt";
    string muoiLam = " lăm";
    var kyTu = number.ToString().ToCharArray();
    switch (kyTu[1])
    {
        case '0':
            if (kyTu[0] == '1')
            {
                return "Mười";
            }
            ketQua = SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangChuc;
            return ketQua;
        case '1':
            if (kyTu[0] == '1')
            {
                return ketQua = SoHangDonVi(10) + SoHangDonVi(int.Parse(kyTu[1].ToString()));
            }
            ketQua = SoHangDonVi(int.Parse(kyTu[0].ToString())) + hangChuc + kyTudacBiet;
            return ketQua;
        case '5':
            if (kyTu[0] == '1')
            {
                return ketQua = SoHangDonVi(10) + muoiLam;
            }
            return SoHangDonVi(int.Parse(kyTu[0].ToString())) + " " + hangChuc + " lăm";
        default:
            if (kyTu[0] == '1')
            {
                return ketQua = SoHangDonVi(10) + SoHangDonVi(int.Parse(kyTu[1].ToString()));
            }
            ketQua = SoHangDonVi(int.Parse(kyTu[0].ToString())) + " " + hangChuc + " " + SoHangDonVi(int.Parse(kyTu[1].ToString()));
            return ketQua;
    }
}

string SoHangDonVi(int number)
{
    switch (number)
    {
        case 0:
            return "Không";
        case 1:
            return "Một";
        case 2:
            return "Hai";
        case 3:
            return "Ba";
        case 4:
            return "Bốn";
        case 5:
            return "Năm";
        case 6:
            return "Sáu";
        case 7:
            return "Bảy";
        case 8:
            return "Tám";
        case 9:
            return "Chín";
        case 10:
            return "Mười";
        default:
            return "";
    }
}

